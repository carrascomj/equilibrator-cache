# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import numpy as np
import pytest

from equilibrator_cache.exceptions import ParseException
from equilibrator_cache.reaction import (
    Reaction,
    create_stoichiometric_matrix_from_reactions,
)


@pytest.mark.parametrize(
    ("formula", "balancing_compound", "ignore_atoms", "can_be_balanced"),
    [
        (
            "kegg.compound:C00002 <=> kegg.compound:C00008 + kegg.compound:C00009",
            "kegg.compound:C00001",
            ("H",),
            True,
        ),
        (
            "6 kegg.compound:C00002 <=> 2 kegg.compound:C00008 + kegg.compound:C00009",
            "kegg.compound:C00001",
            ("H",),
            False,
        ),
        (
            "kegg.compound:C00001 + kegg.compound:C00002 <=> "
            "kegg.compound:C00008 + kegg.compound:C00009",
            "kegg.compound:C00001",
            ("H", "O", "e-"),
            True,
        ),
    ],
)
def test_water_balancing(
    ccache, formula, balancing_compound, ignore_atoms, can_be_balanced
):
    rxn = Reaction.parse_formula(ccache.get_compound, formula)
    cpd = ccache.get_compound(balancing_compound)
    balanced_rxn = rxn.balance_with_compound(compound=cpd, ignore_atoms=ignore_atoms)

    if can_be_balanced:
        assert balanced_rxn is not None
    else:
        assert balanced_rxn is None


@pytest.mark.parametrize(
    "formula",
    [
        "FORMULA_WITHOUT_ARROW",
        "kegg.compound:C00001 Ξ kegg.compound:C00002",
        "NOT_IN_CACHE = null",
        "NONFLOAT kegg.compound:C00001 = null",
    ],
)
def test_parse_formula_fail(ccache, formula):
    with pytest.raises(ParseException):
        Reaction.parse_formula(ccache.get_compound, formula)


@pytest.mark.parametrize(
    (
        "formula",
        "length",
        "can_be_transformed",
        "can_be_balanced",
        "is_balanced",
        "sum_coeff",
        "sum_abs_coeff",
        "exp_hashable",
        "exp_dense",
    ),
    [
        (
            "kegg.compound:C00001 = null",
            1,
            True,
            True,
            False,
            0,
            0,
            ((1, 1.0),),
            [-1.0],
        ),
        (
            "kegg.compound:C00002 + kegg.compound:C00001 <=> "
            "kegg.compound:C00008 + kegg.compound:C00009",
            4,
            True,
            True,
            True,
            1,
            3,
            ((1, 1.0), (2, 1.0), (3, -1.0), (4, -1.0)),
            [-1.0, -1.0, 1.0, 1.0],
        ),
        (
            "kegg.compound:C00002 <=> kegg.compound:C00008 + kegg.compound:C00009",
            3,
            True,
            True,
            False,
            1,
            3,
            ((2, 1.0), (3, -1.0), (4, -1.0)),
            [-1.0, 1.0, 1.0],
        ),
        (
            "6 kegg.compound:C00002 + kegg.compound:C00001 <=> "
            "2 kegg.compound:C00008 + kegg.compound:C00009 + kegg.compound:C00080",
            5,
            False,
            True,
            False,
            -3,
            9,
            ((1, 1.0), (2, 6.0), (3, -2.0), (4, -1.0), (5, -1.0)),
            [-1.0, -6.0, 2.0, 1.0, 1.0],
        ),
    ],
)
def test_parse_formula(
    ccache,
    formula,
    length,
    can_be_transformed,
    can_be_balanced,
    is_balanced,
    sum_coeff,
    sum_abs_coeff,
    exp_hashable,
    exp_dense,
):
    rxn = Reaction.parse_formula(ccache.get_compound, formula)
    assert len(rxn) == length
    assert rxn._sum_coefficients() == sum_coeff
    assert rxn._sum_absolute_coefficients() == sum_abs_coeff

    assert Reaction._hashable_reactants(rxn.sparse) == exp_hashable
    assert hash(rxn) == hash(exp_hashable)
    assert rxn.can_be_transformed() == can_be_transformed

    if can_be_balanced:
        assert rxn.is_balanced() == is_balanced
    else:
        warnings.filterwarnings("error")
        with pytest.raises(Exception):
            rxn.is_balanced()
    assert rxn.reverse().reverse() == rxn
    assert rxn.dense(sorted(rxn.keys())).flat == pytest.approx(exp_dense)


@pytest.mark.parametrize(
    ("formulae", "shape"),
    [
        (
            (
                "kegg.compound:C00002 + kegg.compound:C00001 <=> "
                "kegg.compound:C00008 + kegg.compound:C00009",
                "kegg.compound:C00002 <=> "
                "2 kegg.compound:C00008 + kegg.compound:C00080",
            ),
            (4, 2),
        )
    ],
)
def test_stoichiometric_matrix_building(ccache, formulae, shape):
    reactions = [Reaction.parse_formula(ccache.get_compound, f) for f in formulae]
    stoich = create_stoichiometric_matrix_from_reactions(
        reactions, ccache.is_proton, ccache.is_water, ccache.water
    )
    assert stoich.shape == shape
    assert not ccache.session.dirty
