# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the Compound class behavior."""


import pytest
from hypothesis import example, given
from hypothesis import strategies as st

from equilibrator_cache import (
    Compound,
    CompoundIdentifier,
    CompoundMicrospecies,
    MagnesiumDissociationConstant,
    Registry,
)
from equilibrator_cache.thermodynamic_constants import Q_


@given(id=st.integers(), inchi_key=st.text(), inchi=st.text(), smiles=st.text())
@example(id=None, inchi_key=None, inchi=None, smiles=None)
def test___init__(**kwargs):
    """Ensure a consistent string representation."""
    Compound(**kwargs)


@pytest.mark.parametrize(
    "compound, expected",
    [
        (Compound(), "Compound(id=None, inchi_key=None)"),
        (Compound(id=1), "Compound(id=1, inchi_key=None)"),
        (Compound(inchi_key="sdvwvw"), "Compound(id=None, inchi_key=sdvwvw)"),
        (
            Compound(id=2, inchi_key="sdvwvw"),
            "Compound(id=2, inchi_key=sdvwvw)",
        ),
    ],
)
def test___repr__(compound: Compound, expected: str):
    """Ensure a consistent string representation."""
    assert repr(compound) == expected


@pytest.mark.parametrize(
    "compound, formula",
    [
        (Compound(atom_bag={}), ""),
        (Compound(atom_bag={"C": 1}), "C"),
        (Compound(atom_bag={"C": 1, "H": 0}), "C"),
        (Compound(atom_bag={"C": 1, "H": 0, "O": 2}), "CO2"),
        (Compound(atom_bag={"e-": 4, "C": 1, "O": 2}), "CO2"),
        (Compound(atom_bag={"C": 101, "O": 22}), "C101O22"),
    ],
)
def test_formula(compound: Compound, formula: str):
    """Verify expected formula composition."""
    assert compound.formula == formula


@pytest.fixture(scope="module")
def hco3_compound() -> Compound:
    """Create a Compound object for bicarbonate."""
    return Compound(
        id=60,
        microspecies=[
            CompoundMicrospecies(
                compound_id=60,  # CO3[2-]
                charge=-2,
                number_protons=0,
                is_major=False,
                ddg_over_rt=23.763,
                number_magnesiums=0,
            ),
            CompoundMicrospecies(
                compound_id=60,  # HCO3-
                charge=-1,
                number_protons=1,
                is_major=True,
                ddg_over_rt=0.0,
                number_magnesiums=0,
            ),
            CompoundMicrospecies(
                compound_id=60,  # H2CO3
                charge=0,
                number_protons=2,
                is_major=False,
                ddg_over_rt=-8.06,
                number_magnesiums=0,
            ),
        ],
    )


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, p_mg, expected",
    [
        list(map(Q_, ["10.32", "0.0 M", "298.15 K", "10.0", "57.16 kJ/mol"])),
        list(map(Q_, ["3.5", "0.0 M", "298.15 K", "10.0", "18.25 kJ/mol"])),
    ],
)
def test_transform(
    hco3_compound: Compound,
    p_h: Q_,
    ionic_strength: Q_,
    temperature: Q_,
    p_mg: Q_,
    expected: Q_,
):
    """Verify expected free energy transformation."""
    ddg = hco3_compound.transform(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )

    assert ddg.m_as("kJ/mol") == pytest.approx(expected.m_as("kJ/mol"), abs=0.1)


###############################################################################
# Integration-Style Tests
###############################################################################


@pytest.fixture(scope="function")
def simple_registry(session) -> Registry:
    """Return a registry object that is committed to the database."""
    result = Registry(namespace="Foo", pattern=".+")
    session.add(result)
    session.commit()
    return result


@pytest.fixture(scope="function")
def simple_compound(session) -> Compound:
    """Return a compound object that is committed to the database."""
    cmpnd = Compound(inchi_key="WQZGKKKJIJFFOK-GASJEMHNSA-N")
    session.add(cmpnd)
    session.commit()
    return cmpnd


def test_add_compound_identifier(session, simple_registry, simple_compound):
    """Test that adding a compound identifier is correctly committed."""
    assert session.query(CompoundIdentifier).count() == 0
    simple_compound.identifiers = [
        CompoundIdentifier(registry=simple_registry, accession="glc")
    ]
    session.commit()
    assert session.query(CompoundIdentifier).count() == 1


def test_remove_compound_identifier(session, simple_registry, simple_compound):
    """Test that removing a compound identifier is correctly cascaded."""
    simple_compound.identifiers = [
        CompoundIdentifier(registry=simple_registry, accession="glc")
    ]
    session.commit()
    assert session.query(CompoundIdentifier).count() == 1
    simple_compound.identifiers.pop()
    session.commit()
    assert session.query(CompoundIdentifier).count() == 0


def test_add_microspecies(session, simple_compound):
    """Test that adding a compound microspecies is correctly committed."""
    assert session.query(CompoundMicrospecies).count() == 0
    simple_compound.microspecies = [CompoundMicrospecies()]
    session.commit()
    assert session.query(CompoundMicrospecies).count() == 1


def test_remove_microspecies(session, simple_compound):
    """Test that removing a compound microspecies is correctly cascaded."""
    simple_compound.microspecies = [CompoundMicrospecies()]
    session.commit()
    assert session.query(CompoundMicrospecies).count() == 1
    simple_compound.microspecies.pop()
    session.commit()
    assert session.query(CompoundMicrospecies).count() == 0


def test_add_magnesium_dissociation_constant(session, simple_compound):
    """Test that adding a magnesium dissociation constant is correctly committed."""
    assert session.query(MagnesiumDissociationConstant).count() == 0
    simple_compound.magnesium_dissociation_constants = [
        MagnesiumDissociationConstant(dissociation_constant=1.0)
    ]
    session.commit()
    assert session.query(MagnesiumDissociationConstant).count() == 1


def test_remove_magnesium_dissociation_constant(session, simple_compound):
    """Test that removing a magnesium dissociation constant is correctly cascaded."""
    simple_compound.magnesium_dissociation_constants = [
        MagnesiumDissociationConstant(dissociation_constant=1.0)
    ]
    session.commit()
    assert session.query(MagnesiumDissociationConstant).count() == 1
    simple_compound.magnesium_dissociation_constants.pop()
    session.commit()
    assert session.query(MagnesiumDissociationConstant).count() == 0
