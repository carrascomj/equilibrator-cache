# The MIT License (MIT)
#
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from pathlib import Path

import pytest
import toml
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from equilibrator_cache import (
    Base,
    Compound,
    CompoundCache,
    CompoundIdentifier,
    CompoundMicrospecies,
    CompoundName,
    Registry,
)


root_dir = Path(__file__).parent
with (root_dir / "seeds" / "registries.toml").open() as handle:
    registries = toml.load(handle)["registries"]
with (root_dir / "seeds" / "compounds.toml").open() as handle:
    compounds = toml.load(handle)["compounds"]


Session = sessionmaker()


@pytest.fixture(scope="session")
def orm_session():
    result = Session(bind=create_engine("sqlite:///:memory:"))
    Base.metadata.create_all(result.bind)
    return result


@pytest.fixture(scope="session")
def orm_registries(orm_session):
    result = [Registry(**attrs) for attrs in registries]
    orm_session.add_all(result)
    orm_session.commit()
    return result


@pytest.fixture(scope="session")
def orm_compounds(orm_registries, orm_session):
    result = []
    for attrs in compounds:
        microspecies = [
            CompoundMicrospecies(**a) for a in attrs.pop("microspecies", [])
        ]
        identifiers = [CompoundIdentifier(**a) for a in attrs.pop("identifiers", [])]
        names = [CompoundName(**a) for a in attrs.pop("names", [])]
        cmpnd = Compound(**attrs)
        # Convert TOML-specific mapping type to dict so that SQLAlchemy can handle it.
        cmpnd.atom_bag = dict(cmpnd.atom_bag)
        cmpnd.microspecies = microspecies
        cmpnd.identifiers = identifiers
        cmpnd.names = names
        result.append(cmpnd)
    orm_session.add_all(result)
    orm_session.commit()
    return result


@pytest.fixture(scope="session")
def ccache(orm_compounds, orm_session):
    return CompoundCache(engine=orm_session.bind)
